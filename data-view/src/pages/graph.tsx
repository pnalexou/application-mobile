import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import {ChartSelect} from "../components/DataSelect";
import Parser from "../components/Parser";

import React from "react";
import "./graph.css";

const Graph = () => {
  const [donnees, setDonnees] = React.useState<Array<any>>([]);

  return (
    <>
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonButtons slot="start">
              <IonMenuButton />
            </IonButtons>
            <IonTitle>Graphiques</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonContent id="resume" slot="fixed" scrollY={false}>
          <IonHeader collapse="condense">
            <IonToolbar>
              <IonTitle size="large">Résumés statistiques</IonTitle>
            </IonToolbar>
          </IonHeader>
          <div id="parser">
            <Parser onNewData={(data) => setDonnees(data)} />
          </div>

          <div id="chart">
            {donnees.length !== 0 ? <ChartSelect donnees={donnees} /> : ""}
          </div>
        </IonContent>
      </IonPage>
    </>
  );
};

export default Graph;
