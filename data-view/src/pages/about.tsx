import {
  IonGrid,
  IonRow,
  IonCol,
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonText,
} from "@ionic/react";
import React from "react";
import "./about.css";

const About: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>À propos de Data View</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">À propos de Data View</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonGrid no-padding>
          <IonRow>
            <IonCol class="flex">
              <IonText color="primary">
                <div id="about">
                  <h1 id="about">Data View</h1>
                </div>
              </IonText>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol class="flex">
              <IonText color="dark">
                <h4>Version bêta</h4>
              </IonText>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol class="flex">
              <IonText color="dark">
                <h6>0.9</h6>
              </IonText>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol class="flex">
              <IonText color="dark">
                <h6>Créé par Alexandre Peter Nguema</h6>
              </IonText>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default About;
