import {
  IonButtons,
  IonSegment,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonSegmentButton,
  IonLabel,
  IonIcon,
} from "@ionic/react";
import SummaryTable from "../components/DataTable";
import VarSelect from "../components/DataSelect";
import Parser from "../components/Parser";

import React from "react";
import "./stats.css";
import { filterOutline, documentTextOutline } from "ionicons/icons";

const Stats = () => {
  const [donnees, setDonnees] = React.useState<Array<any>>([]);
  const [value, setValue] = React.useState("global");

  return (
    <>
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonButtons slot="start">
              <IonMenuButton />
            </IonButtons>
            <IonTitle>Résumés statistiques</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonContent id="resume" scrollY={false}>
          <IonHeader collapse="condense">
            <IonToolbar>
              <IonTitle size="large">Résumés statistiques</IonTitle>
            </IonToolbar>
          </IonHeader>
          <div id="parser">
            <Parser onNewData={(data) => setDonnees(data)} />
          </div>

          <IonSegment
            onIonChange={(e) => setValue(e.detail.value!)}
            value={value}
          >
            <IonSegmentButton value="global">
              <IonLabel>Global</IonLabel>
              <IonIcon icon={documentTextOutline} />
            </IonSegmentButton>
            <IonSegmentButton value="variables">
              <IonLabel>Variables</IonLabel>
              <IonIcon icon={filterOutline} />
            </IonSegmentButton>
          </IonSegment>

          <div id="stats">
            {value === "global" ? (
              donnees.length !== 0 ? (
                <SummaryTable donnees={donnees} />
              ) : (
                ""
              )
            ) : donnees.length !== 0 ? (
              <VarSelect donnees={donnees} />
            ) : (
              ""
            )}
          </div>
        </IonContent>
      </IonPage>
    </>
  );
};

export default Stats;
