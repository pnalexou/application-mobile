import {
  IonGrid,
  IonRow,
  IonCol,
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonToolbar,
  IonText,
  IonButton,
} from "@ionic/react";
import React from "react";
import "./home.css";

const Home: React.FC = () => {
  return (
    <IonPage>
      <IonHeader></IonHeader>

      <IonContent
        forceOverscroll
        fullscreen
        class="home"
        scrollY={false}
        no-padding
        no-bounce
      >
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton color="light" />
          </IonButtons>
        </IonToolbar>

        <IonGrid no-padding>
          <IonRow>
            <IonCol class="flex">
              <IonText color="light">
                <div id="home">
                  <h1 id="home">Data View</h1>
                </div>
              </IonText>
            </IonCol>
          </IonRow>
        </IonGrid>
        <IonButton
          href="/page/stats"
          class="home"
          color="secondary"
          shape="round"
          expand="block"
        >
          Statistiques descriptives
        </IonButton>
        <IonButton
          href="/page/charts"
          class="home"
          color="secondary"
          shape="round"
          expand="block"
        >
          Graphiques
        </IonButton>
      </IonContent>
    </IonPage>
  );
};

export default Home;
