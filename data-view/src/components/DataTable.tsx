import React from "react";
import { IonRow, IonCol } from "@ionic/react";
import "./DataTable.css";

interface Props {
  donnees: any[];
}

interface varProps {
  donnees: any[];
  varIndex: number;
}

const SummaryTable: React.FC<Props> = ({ donnees }) => {
  var tab: any;
  const data = donnees.slice(1);

  tab = (
    <>
      <div>
        <IonRow>
          <IonCol id="tab" size="9" class="headCol">
            Nombre d'observations
          </IonCol>
          <IonCol id="tab">{data.length - 1}</IonCol>
        </IonRow>
        <IonRow>
          <IonCol id="tab" size="9" class="headCol">
            Nombre de variables
          </IonCol>
          <IonCol id="tab">{data[0].data.length}</IonCol>
        </IonRow>
      </div>
    </>
  );

  return tab;
};

export const VarTable = ({ donnees, varIndex }: varProps) => {
  var tab: any;
  const data = donnees.slice(1, donnees.length - 1);
  const distinct = (value: any, index: any, self: any) => {
    return self.indexOf(value) === index;
  };


  const mod = data.map((element) => element.data[varIndex]);
  const type = typeof mod[0];
  const distinctMod = mod.filter(distinct);

  const average = (array: number[]) =>
    array.reduce((a, b) => a + b) / array.length;
  const median = (arr: number[]) => {
    const mid = Math.floor(arr.length / 2),
      nums = [...arr].sort((a, b) => a - b);
    return arr.length % 2 !== 0 ? nums[mid] : (nums[mid - 1] + nums[mid]) / 2;
  };

  if (type === "string") {
    tab = (
      <>
        <IonRow>
          <IonCol id="tab" size="7" class="headCol">
            Type de variable
          </IonCol>
          <IonCol id="tab">Qualitatif</IonCol>
        </IonRow>
        <IonRow>
          <IonCol id="tab" size="7" class="headCol">
            Nombre de modalités
          </IonCol>
          <IonCol id="tab">{distinctMod.length}</IonCol>
        </IonRow>
        <IonRow>
          <IonCol id="tab" size="7" class="headCol">
            Modalités
          </IonCol>
          <IonCol id="tab">
            {distinctMod &&
              distinctMod.map((e) => (
                <>
                  {" "}
                  <span>{e}</span> ;
                </>
              ))}
          </IonCol>
        </IonRow>
      </>
    );
  } else {
    tab = (
      <>
        <IonRow>
          <IonCol id="tab" size="6" class="headCol">
            Type de variable
          </IonCol>
          <IonCol id="tab">Quantitatif</IonCol>
        </IonRow>
        <IonRow>
          <IonCol id="tab" size="6" class="headCol">
            Moyenne
          </IonCol>
          <IonCol id="tab">{average(mod)}</IonCol>
        </IonRow>
        <IonRow>
          <IonCol id="tab" size="6" class="headCol">
            Médiane
          </IonCol>
          <IonCol id="tab">{median(mod)}</IonCol>
        </IonRow>
        <IonRow>
          <IonCol id="tab" size="6" class="headCol">
            Max
          </IonCol>
          <IonCol id="tab">{Math.max.apply(null, mod)}</IonCol>
        </IonRow>
        <IonRow>
          <IonCol id="tab" size="6" class="headCol">
            Min
          </IonCol>
          <IonCol id="tab">{Math.min.apply(null, mod)}</IonCol>
        </IonRow>
      </>
    );
  }

  return tab;
};

export default SummaryTable;
