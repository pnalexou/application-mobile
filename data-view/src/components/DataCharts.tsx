import React from "react";
import Chart from "react-apexcharts";
import { IonGrid, IonRow, IonCol, IonText } from "@ionic/react";

interface Props {
  donnees: any[];
  varIndex: number;
}

interface Config {
  options: any;
  series: any[];
}

const BarChart: React.FC<Props> = ({ donnees, varIndex }) => {
  var chart: any;
  console.log(donnees.slice(0, 1)[varIndex]);
  var config: Config;

  const head = donnees.slice(0, 1);
  const data = donnees.slice(1, donnees.length - 1);
  const distinct = (value: any, index: any, self: any) => {
    return self.indexOf(value) === index;
  };

  const mod = data.map((element: any) => element.data[varIndex]);
  const type = typeof mod[0];
  const distinctMod = mod.filter(distinct);

  const count = mod.reduce(function (obj: any, item: any) {
    obj[item] = (obj[item] || 0) + 1;
    return obj;
  }, {});

  const series = distinctMod.map((e: any) => count[e]);

  config = {
    options: {
      chart: {
        id: "basic-bar",
      },
      xaxis: {
        categories: distinctMod,
        title: {
          text: "Modalités",
          offsetX: 0,
          offsetY: 0,
          style: {
            color: undefined,
            fontSize: "12px",
            fontFamily: "Helvetica, Arial, sans-serif",
            fontWeight: 600,
            cssClass: "apexcharts-xaxis-title",
          },
        },
      },
      axis: {
        categories: distinctMod,
        title: {
          text: "Modalités",
          offsetX: 0,
          offsetY: 0,
          style: {
            color: undefined,
            fontSize: "12px",
            fontFamily: "Helvetica, Arial, sans-serif",
            fontWeight: 600,
            cssClass: "apexcharts-xaxis-title",
          },
        },
      },
      title: {
        text: `Répartition de ${head[0].data[varIndex]}`,
        align: "center",
        margin: 10,
        offsetX: 0,
        offsetY: 0,
        floating: false,
        style: {
          fontSize: "14px",
          fontWeight: "bold",
          fontFamily: undefined,
          color: "#263238",
        },
      },
    },
    series: [
      {
        name: "Effectif",
        data: series,
      },
    ],
  };

  if (type === "string") {
    if (typeof config == "undefined") {
      console.log("Aucune variable sélectionnée");
    } else {
      chart = (
        <Chart
          options={config.options}
          series={config.series}
          type="bar"
          height="auto"
        />
      );
    }
  } else {
    chart = (
      <>
        <IonGrid no-padding>
          <IonRow>
            <IonCol class="flex">
              <IonText color="dark">
                <h1>
                  Graphiques indisponibles pour les variables quantitatives
                </h1>
              </IonText>
            </IonCol>
          </IonRow>
        </IonGrid>
        <img
          alt="decoration-charts"
          src={require("../images/graph_load.png")}
        />
      </>
    );
  }

  return chart;
};

export default BarChart;
