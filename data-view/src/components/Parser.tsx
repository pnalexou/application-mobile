import { CSVReader } from "react-papaparse";
import React from "react";

interface Props {
  onNewData: (data: any[]) => void;
}

const Parser = ({ onNewData }: Props): JSX.Element => {
  const handleOnDrop = (data: any[]) => {
    onNewData(data);
    console.log("---------------------------");
    console.log(data);
    console.log("---------------------------");
  };

  const handleOnError = (err: any, file: any, inputElem: any, reason: any) => {
    console.log(err);
  };

  const handleOnRemoveFile = (data: any[]) => {
    onNewData([]);
    console.log("---------------------------");
    console.log(data);
    console.log("---------------------------");
  };

  return (
    <>
      <CSVReader
        onDrop={handleOnDrop}
        onError={handleOnError}
        config={{
          dynamicTyping: true,
        }}
        addRemoveButton
        onRemoveFile={handleOnRemoveFile}
      >
        <span>Sélectionner un fichier à analyser</span>
      </CSVReader>
    </>
  );
};

export default Parser;
