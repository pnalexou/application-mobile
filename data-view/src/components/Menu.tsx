import {
  IonButtons,
  IonHeader,
  IonTitle,
  IonMenuButton,
  IonContent,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonMenuToggle,
  IonToolbar,
} from "@ionic/react";

import React from "react";
import { useLocation } from "react-router";
import {
  informationCircleOutline,
  homeOutline,
  barChartOutline,
  pieChartSharp,
} from "ionicons/icons";
import "./Menu.css";

const routes = {
  appPages: [
    { title: "Démarrer", path: "/page/home", icon: homeOutline },
    { title: "Statistiques", path: "/page/stats", icon: barChartOutline },
    { title: "Graphiques", path: "/page/charts", icon: pieChartSharp },
  ],
  aboutPage: [
    { title: "À propos", path: "/page/about", icon: informationCircleOutline },
  ],
};

interface Pages {
  title: string;
  path: string;
  icon: string;
  routerDirection?: string;
}

const Menu: React.FC = () => {
  const location = useLocation();

  function renderlistItems(list: Pages[]) {
    return list
      .filter((route) => !!route.path)
      .map((p) => (
        <IonMenuToggle key={p.title} auto-hide="false">
          <IonItem
            detail={false}
            routerLink={p.path}
            routerDirection="none"
            className={
              location.pathname.startsWith(p.path) ? "selected" : undefined
            }
          >
            <IonIcon slot="start" icon={p.icon} />
            <IonLabel>{p.title}</IonLabel>
          </IonItem>
        </IonMenuToggle>
      ));
  }

  return (
    <>
      <IonMenu type="overlay" contentId="main">
        <IonContent class="menu" forceOverscroll={false}>
          <IonHeader>
            <IonToolbar id="menu">
              <IonButtons slot="start">
                <IonMenuButton />
              </IonButtons>
              <IonTitle>Data View</IonTitle>
            </IonToolbar>
          </IonHeader>
          <IonList lines="none">
            <IonListHeader>Révélez le potentiel de vos données</IonListHeader>
            {renderlistItems(routes.appPages)}
          </IonList>
          <IonList lines="full">{renderlistItems(routes.aboutPage)}</IonList>
        </IonContent>
      </IonMenu>
    </>
  );
};

export default Menu;
