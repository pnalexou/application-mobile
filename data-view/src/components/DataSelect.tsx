import React from "react";
import {
  IonContent,
  IonLabel,
  IonItem,
  IonSelect,
  IonSelectOption,
} from "@ionic/react";
import { VarTable } from "../components/DataTable";
import BarChart from "../components/DataCharts";

interface Props {
  donnees: any[];
}

const VarSelect: React.FC<Props> = ({ donnees }) => {
  const [selected, setSelected] = React.useState<string>("");

  const head = donnees.slice(0, 1);

  console.log(head);

  return (
    <>
      <IonItem>
        <IonLabel>Choissez une variable</IonLabel>
        <IonSelect
          value={selected}
          onIonChange={(e) => setSelected(e.detail.value)}
        >
          {head &&
            head[0].data.map((elt: any) => (
              <IonSelectOption value={elt}>{elt}</IonSelectOption>
            ))}
        </IonSelect>
      </IonItem>

      <IonContent>
        {donnees.length !== 0 && selected !== "" ? (
          <div id="stats">
            <VarTable
              donnees={donnees}
              varIndex={head[0].data.indexOf(selected)}
            />
          </div>
        ) : (
          ""
        )}
      </IonContent>
    </>
  );
};

export const ChartSelect: React.FC<Props> = ({ donnees }) => {
  const [selected, setSelected] = React.useState<string>("");

  const head = donnees.slice(0, 1);

  console.log(head);

  return (
    <>
      <IonItem>
        <IonLabel>Choissez une variable</IonLabel>
        <IonSelect
          value={selected}
          onIonChange={(e) => setSelected(e.detail.value)}
        >
          {head &&
            head[0].data.map((elt: any) => (
              <IonSelectOption value={elt}>{elt}</IonSelectOption>
            ))}
        </IonSelect>
      </IonItem>

      <IonContent>
        {donnees.length !== 0 && selected !== "" ? (
          <div id="stats">
            <BarChart
              donnees={donnees}
              varIndex={head[0].data.indexOf(selected)}
            />
          </div>
        ) : (
          ""
        )}
      </IonContent>
    </>
  );
};

export default VarSelect;
