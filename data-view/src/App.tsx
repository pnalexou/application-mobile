import Menu from "./components/Menu";
import Home from "./pages/home";
import Stats from "./pages/stats";
import Graph from "./pages/graph";
import About from "./pages/about";
import React from "react";
import { IonApp, IonRouterOutlet, IonSplitPane } from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import { Redirect, Route } from "react-router-dom";
import "./App.css";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

import "@swimlane/ngx-datatable/index.css";
import "@swimlane/ngx-datatable/themes/dark.css";
import "@swimlane/ngx-datatable/themes/bootstrap.css";
import "@swimlane/ngx-datatable/themes/material.css";
import "@swimlane/ngx-datatable/assets/icons.css";



/* Theme variables */
import "./theme/variables.css";



const App: React.FC = () => {
  return (
    <IonApp>
      <IonReactRouter>
        <IonSplitPane contentId="main">
          <Menu />
          <IonRouterOutlet id="main">
            <Route path="/page/home" component={Home} />
            <Route path="/page/stats" component={Stats} exact={true} />
            <Route path="/page/charts" component={Graph} />
            <Route path="/page/about" component={About} />
            <Route
              path="/"
              render={() => <Redirect to="/page/home" />}
              exact={true}
            />
          </IonRouterOutlet>
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
