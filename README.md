# Data View Application

Data View is an application that allows you to quickly obtain statistical summaries from a CSV file. Make sure that the CSV file :

- has the label of the variables on the first line
- ​​has a point for the decimal separator of numerical values

You can test the app with the CSV file "loisir"  located in "data-view / src / data".

The apk file (data-view.apk) is located in the root.

## Table of Contents
- [Getting Started](#getting-started)
- [App Preview](#app-preview)

## Getting Started

* [Download the installer](https://nodejs.org/) for Node LTS.
* Place in the folder data-view `cd .\data-view\`
* Install the ionic CLI globally: `npm install -g ionic`
* Clone this repository: `git clone https://github.com/ionic-team/ionic-react-conference-app.git`.
* Run `npm install` from the project root.
* Run `ionic serve` in a terminal from the project root.
* Profit. :tada:

## App Preview

### Home

| Material Design  | 
| -----------------| 
| ![Home](/resources/screenshots/home.png) |


### Side menu

| Material Design  | 
| -----------------| 
|![Side menu](/resources/screenshots/side_lenu.png) |

### Statistics page

| Material Design  | 
| -----------------| 
| ![Statistics page](/resources/screenshots/menu_stat.png) |



